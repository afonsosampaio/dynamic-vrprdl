#ifndef ROUTE_H
#define ROUTE_H

#include <map>
#include <queue>
#include <iostream>

#include "../Headers/data.h"
#include "../Headers/TimeWindows.h"

using namespace std;

class Route
{
  private:
    nodeInfo* first_visit;
    nodeInfo* last_visit;  //after and before the depot, respectively
    // int start, end;
    double route_cost;
    int length;
    int load;
    int waiting;
    instanceData* problemdata;
    vector<Customer>* customers;
    vector<Location>* locations;
  public:
    Route(instanceData*);
    bool evaluateInsertion(nodeInfo*, priority_queue<insertion>&);
    void insertNode(nodeInfo*, nodeInfo*, nodeInfo*);
    void update_earliest();
    void update_latest();
    int getLength();
    bool step();
    void assignCustomers();
    int getReturnTime();
    int getLatestDispatch();
    int getWaitingTime();
    nodeInfo start_depot, end_depot;
    friend ostream& operator<< (std::ostream&, Route&);
};

struct less_than_key
{
  inline bool operator() (Route* route1, Route* route2)
  { return (route1->getLength() > route2->getLength()); }
};

#endif
