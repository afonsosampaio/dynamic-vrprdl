#ifndef TW_H
#define TW_H

#include <iostream>

using namespace std;

template <class TT>
class TimeWindows;

template <class TT>
ostream& operator<< (ostream& out, TimeWindows<TT>& tw);

template <class TT>
class TimeWindows
{
    private:
        TT lower, upper;
    public:
        TimeWindows() : lower(0), upper(0) {}
        TimeWindows(TT l, TT u) : lower(l), upper(u) {}
        TT length() { return upper - lower; }
        void set(TT l, TT u) { lower = l; upper = u; }
        void setStart(TT l) {lower=l;}
        void setEnd(TT u) {upper=u;}
        TT getStart() const { return lower; }
        TT getEnd() const { return upper; }
        TT Overlaping(TimeWindows<TT>& tw);
        TT& operator= (TimeWindows<TT>&);
        bool operator== (TimeWindows<TT>&);
        friend ostream& operator<< <>(std::ostream& out, TimeWindows& tw);
};

template <class TT>
ostream& operator<< (std::ostream& out, TimeWindows<TT>& tw)
{
    out<<"["<<tw.getStart()<<" "<<tw.getEnd()<<"]";
    return out;
}

template <class TT>
TT TimeWindows<TT>::Overlaping(TimeWindows<TT>& tw)
{
    TT latest_lower = tw.getStart() < this->getStart() ? this->getStart() : tw.getStart();
    TT earliest_upper = tw.getEnd() > this->getEnd() ? this->getEnd() : tw.getEnd();
    return earliest_upper - latest_lower > 0 ? earliest_upper - latest_lower : 0;
}

template <class TT>
TT& TimeWindows<TT>::operator= (TimeWindows<TT>& tw)
{
  lower = tw.lower;
  upper = tw.upper;
}

template <class TT>
bool TimeWindows<TT>::operator== (TimeWindows<TT>& tw)
{
  if(lower == tw.lower and upper == tw.upper)
    return true;
  return false;
}

#endif
