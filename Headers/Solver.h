#ifndef SOLVER_H
#define SOLVER_H

#include <vector>
#include <algorithm>
#include <iostream>

#include "../Headers/data.h"
#include "../Headers/Route.h"

using namespace std;

class Solver
{
  friend class Solution;
  private:
    instanceData* problemdata;
    vector<Customer>* customers;
    vector<Location>* locations;
    Solution* solution;
    priority_queue<insertion> insertions_pq;
  public:
    Solver(instanceData*);
    int solve(vector<int>*, vector<Route*>*, int);
    int initial(vector<Route*>*);
    void checkInstance();
    // void update_solution();
};

/*
*/
class Solution
{
  friend class Solver;
  private:
    int size;                       //number of nodes
    vector<nodeInfo*> planned_visits;
    vector<Route*> routing_plan;
    Solver* solver;
    void advance_time();
  public:
    Solution(Solver*);
};

#endif
