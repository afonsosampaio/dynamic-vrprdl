#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <vector>


#include "../Headers/user_exceptions.h"
#include "../Headers/TimeWindows.h"
#include "../Headers/data.h"

using namespace std;

/*A parser for the original VRPRDL instances proposed in
 *Reyes, D. Savelsbergh, M. Toriello, A. Vehicle Routing with Roaming Delivery Locations. Transportation Research part C.
**/
class Parser
{
  public:
    Parser(string, instanceData*);
  private:
};

#endif
