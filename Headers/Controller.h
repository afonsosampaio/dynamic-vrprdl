#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <vector>
#include <map>
#include <algorithm>
#include <iostream>

#include "../Headers/Observer.h"
#include "../Headers/parser.h"
#include "../Headers/Solver.h"

class Controller : public Observer
{
  private:
    int id;
    instanceData* problemdata;
    vector<Customer>* customers;
    vector<Location>* locations;
    vector<int> idx_locations;
    map<int, trackCustomer> tracking;
    vector<Vehicle> vehicles;
    vector<Route*> routes;
    int serviced_customers;

    bool end_of_process;
    int time_progress;
    Solver* solver;

  public:
    Controller(int, instanceData*);
    void exec();
    void step();

    virtual void update(Status*) override;
};

#endif
