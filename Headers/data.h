#ifndef DATA_H
#define DATA_H

#include <vector>
#include <math.h>

#include "../Headers/TimeWindows.h"

#define TIME_UNIT 500  //NOTE: in milliseconds

#define FLEET_SIZE 5

//EVENTS
#define END_OF_HORIZON 1
#define CUSTOMER_LOCATION 2

class Solution;
class Route;

typedef struct Customer
{
  vector<int> itinerary;
  int demand;
  int current_location;
  Route* service_route;
  bool serviced = false;

  //for tracking a travelling customer
  double t;
  double step;
  double xcoord;
  double ycoord;
  Customer(int d) : demand(d), current_location(0), service_route(nullptr), serviced(false) {}
} Customer;

typedef struct Location
{
  int id;
  int customer; //the associated customer serviced at this location
  double xcoord;
  double ycoord;
  TimeWindows<int> service;
  bool released;
  bool departed;
  Location(int i, int c, int x, int y) : id(i), customer(c), xcoord(x), ycoord(y), released(false), departed(false) {}
} Location;

typedef struct Vehicle
{
  int ID;
  int capacity;
  int current_route;
  bool dispatched;  //0-at depot, 1-en route
  int return_time;
  Vehicle(int id, int Q) : ID(id), capacity(Q), dispatched(false), current_route(0), return_time(0) {}
  vector<Route*> planned_routes;
} Vehicle;

typedef struct Status
{
  int time_epoch;
  int event;
  vector<int> available_customers;
  vector<int> travelling_customers;
} Status;

typedef struct trackCustomer
{
  int customerID;
  int time_budget;
  bool expired;
  //Route* route; //if assigend to a route
  trackCustomer(int id, int time) : customerID(id), time_budget(time), expired(false) {}
} trackCustomer;

//a visited location in a route
typedef struct nodeInfo
{
  int customer; //
  int location;
  TimeWindows<int> service;

  nodeInfo* pred;
  nodeInfo* succ;
  nodeInfo(int c, int l) : customer(c), location(l), pred(nullptr), succ(nullptr) {}
  nodeInfo() : customer(-1), location(-1), pred(nullptr), succ(nullptr) {}
} nodeInfo;

struct insertion
{
  double cost;
  nodeInfo* before;
  nodeInfo* after;
  Route* route;
  insertion() : route(nullptr), cost(0), before(nullptr), after(nullptr) {}//route1(nullptr)
  insertion(Route* r, double c, nodeInfo* b, nodeInfo* a) : route(r), cost(c), before(b), after(a) {}
};

typedef struct instanceData
{
  vector<Customer>* customers;
  vector<Location>* locations;
  int NR_CUST;
  int NR_LOC;
  int TIME_HORIZON;
  int VEHIC_CAP;
  int START_DEPOT;
  int TARGET_DEPOT;
  //TODO: empty constructor
  instanceData(vector<Customer>* cust, vector<Location>* locs) : customers(cust), locations(locs) {};
  // problemData(vector<Customer>* cust, vector<Location>* locs, int nc, int nl, int h, in q) customers(cust), locations(locs), NR_CUST(nc), NR_LOC(nl), TIME_HORIZON(h), VEHIC_CAP(q) {};
} instanceData;

bool operator <(const insertion&, const insertion&);

//FIXME
inline int distance(int coord_x1, int coord_y1, int coord_x2, int coord_y2)
{ return 2*ceil(sqrt((coord_x1-coord_x2)*(coord_x1-coord_x2) + (coord_y1-coord_y2)*(coord_y1-coord_y2))); }


#endif
