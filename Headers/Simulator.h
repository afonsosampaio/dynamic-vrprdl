#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <vector>
#include <algorithm>
#include <iostream>

#include "../Headers/Subject.h"
#include "../Headers/Observer.h"
#include "../Headers/parser.h"
#include "../Headers/data.h"

using namespace std;

class Simulator : public Subject
{
  private:
    vector<Observer*> observers;
    instanceData* problemdata;
    vector<Customer>* customers;
    vector<Location>* locations;

    int time_progress;
    bool end_of_process;

    Status system_status;
    // vector<int> new_customers;

  public:
    Simulator(instanceData*);
    int step();
    void update_system();

    /*implement the subject interface*/
    void registerObserver(Observer *observer) override;
    void removeObserver(Observer *observer) override;
    void notifyObservers(Status*) override;


    /**
    * Set the new state of the system
    */

    /*Simulates an VRPRDL instance*/
    void exec();
};

#endif
