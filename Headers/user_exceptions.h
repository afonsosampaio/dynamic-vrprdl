#include <iostream>
#include <exception>

using namespace std;

class FileNotFound_Exception : public exception{
    public:
        const char* what() const throw()
        {
            return "File Not Found!";
        }
};
