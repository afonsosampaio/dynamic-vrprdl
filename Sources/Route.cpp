#include "../Headers/Route.h"

Route::Route(instanceData* data) : problemdata(data), first_visit(nullptr), last_visit(nullptr), route_cost(0.0), length(0), load(0), waiting(0)
{
  customers = problemdata->customers;
  locations = problemdata->locations;

  start_depot.customer = problemdata->START_DEPOT;
  start_depot.location = problemdata->START_DEPOT;

  end_depot.customer = problemdata->TARGET_DEPOT;
  end_depot.location = problemdata->NR_LOC-1;
}

bool Route::evaluateInsertion(nodeInfo* node, priority_queue<insertion>& insertions)
{
  nodeInfo* prev = &start_depot;
  nodeInfo* next = first_visit;

  int prev_earliest, earliest;
  int next_latest, latest;

  int travel_prev, travel_next, travel_direct;

  double x1,y1,x2,y2,x3,y3;

  if(next == nullptr)  //empty route
  {
    prev_earliest = prev->service.getStart(); //start depot
    x1 = locations->at(prev->location).xcoord;
    y1 = locations->at(prev->location).ycoord;
    x2 = locations->at(node->location).xcoord;
    y2 = locations->at(node->location).ycoord;
    x3 = locations->at(end_depot.location).xcoord;
    y3 = locations->at(end_depot.location).ycoord;

    travel_prev = distance(x1,y1,x2,y2);
    earliest = max(node->service.getStart(), prev_earliest + travel_prev);

    next_latest = end_depot.service.getEnd();
    travel_next = distance(x2,y2,x3,y3);
    latest = min(node->service.getEnd(), next_latest - travel_next);

    if(earliest <= latest)  //feasible
    {
      insertions.push(insertion(this, travel_prev + travel_next, &start_depot, &end_depot));
      return true;
    }
  }

  //contains at least one node
  while(next != nullptr)
  {
    x1 = locations->at(prev->location).xcoord;
    y1 = locations->at(prev->location).ycoord;
    x2 = locations->at(node->location).xcoord;
    y2 = locations->at(node->location).ycoord;
    x3 = locations->at(next->location).xcoord;
    y3 = locations->at(next->location).ycoord;

    prev_earliest = prev->service.getStart();
    travel_prev = distance(x1,y1,x2,y2);
    earliest = max(node->service.getStart(), prev_earliest + travel_prev);

    next_latest = next->service.getEnd();
    travel_next = distance(x2,y2,x3,y3);
    latest = min(node->service.getEnd(), next_latest - travel_next);

    if(earliest <= latest and load+customers->at(node->customer).demand <= problemdata->VEHIC_CAP)  //feasible
    {
      travel_direct = distance(x1,y1,x3,y3);
      insertions.push(insertion(this, travel_prev + travel_next - travel_direct, prev, next));
    }

    prev = next;
    next = next->succ;
  }

  return !insertions.empty();
}

void Route::insertNode(nodeInfo* node, nodeInfo* before, nodeInfo* after)
{
  // cout<<node->customer<<" between "<<before->customer<<" and "<<after->customer<<endl;
  length++;
  load += customers->at(node->customer).demand;
  node->pred = before;
  node->succ = after;
  before->succ = after->pred = node;
  if(before->customer == problemdata->START_DEPOT)
    first_visit = node;
  if(after->customer == problemdata->TARGET_DEPOT)
    last_visit = node;
}

void Route::assignCustomers()
{
  nodeInfo* prev;
  nodeInfo* next;

  prev = &start_depot;
  next = first_visit;

  while(next != nullptr)
  {
    if(next != &end_depot)
      customers->at(next->customer).service_route = this;     //TODO: is it a probelm accessing/modifying in parallel?

    prev = next;
    next = next->succ;
  }
}

void Route::update_earliest()
{
  nodeInfo* prev;
  nodeInfo* next;
  double x1,y1,x2,y2;
  int prev_earliest;
  int travel;

  prev = &start_depot;
  next = first_visit;

  waiting = 0;

  while(next != nullptr)
  {
    x1 = locations->at(prev->location).xcoord;
    y1 = locations->at(prev->location).ycoord;
    x2 = locations->at(next->location).xcoord;
    y2 = locations->at(next->location).ycoord;

    prev_earliest = prev->service.getStart();
    travel = distance(x1,y1,x2,y2);
    next->service.setStart(max(next->service.getStart(), prev_earliest + travel));

    //waiting time assuming departure at earliest time
    waiting += max(0, next->service.getStart() - (prev->service.getStart() + travel));

    prev = next;
    next = next->succ;
  }
}

void Route::update_latest()
{
  nodeInfo* prev;
  nodeInfo* next;
  double x1,y1,x2,y2;
  int next_latest;
  int travel;

  next = &end_depot;
  prev = last_visit;

  while(prev != nullptr)
  {
    x1 = locations->at(prev->location).xcoord;
    y1 = locations->at(prev->location).ycoord;
    x2 = locations->at(next->location).xcoord;
    y2 = locations->at(next->location).ycoord;

    next_latest = next->service.getEnd();
    travel = distance(x1,y1,x2,y2);
    prev->service.setEnd(min(prev->service.getEnd(), next_latest - travel));
    next = prev;
    prev = prev->pred;
  }
}

int Route::getLength()
{ return length; }

bool Route::step()
{
  nodeInfo* prev;
  nodeInfo* next;
  double x1,y1,x2,y2;
  int prev_earliest;
  int travel;

  prev = &start_depot;
  next = first_visit;

  //NOTE:for any route, start_depot/service.getStart() gives the current time!
  start_depot.service.setStart(start_depot.service.getStart()+1);

  while(next != nullptr)
  {
    x1 = locations->at(prev->location).xcoord;
    y1 = locations->at(prev->location).ycoord;
    x2 = locations->at(next->location).xcoord;
    y2 = locations->at(next->location).ycoord;

    prev_earliest = prev->service.getStart();
    travel = distance(x1,y1,x2,y2);
    next->service.setStart(max(next->service.getStart(), prev_earliest + travel));
    prev = next;
    next = next->succ;
  }

  //NOTE:for any route, start_depot/service.getStart() gives the current time!
  if(start_depot.service.getStart() == start_depot.service.getEnd())  //last dispacht time!
    return true;

  return false;
}

int Route::getReturnTime()
{ return end_depot.service.getStart(); }

int Route::getLatestDispatch()
{ return start_depot.service.getStart(); }

int Route::getWaitingTime()
{ return waiting; }

ostream& operator<< (std::ostream& out, Route& rt)
{
  nodeInfo* prev;
  nodeInfo* next;

  prev = &rt.start_depot;
  next = rt.first_visit;

  out<<prev->customer<<"("<<prev->location<<") ["<<prev->service.getStart()<<","<<prev->service.getEnd()<<"]-->";
  while(next != nullptr)
  {
    out<<next->customer<<"("<<next->location<<") ["<<next->service.getStart()<<","<<next->service.getEnd()<<"]-->";
    next = next->succ;
  }
  out<<" load:"<<rt.load;
  return out;
}
