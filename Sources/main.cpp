#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <queue>
#include <map>
#include <thread>
#include <math.h>
#include <unistd.h>

#include "../Headers/parser.h"
#include "../Headers/Simulator.h"
#include "../Headers/Controller.h"

using namespace std;

int main(int argc, char* argv[])
{
  // Parse command line arguments
  string instance_file;
  char param;
  while((param = getopt(argc, argv, "f:")) != -1)
  {
    switch(param)
    {
      case 'f': //instance file
        instance_file = string(optarg);
      break;
    }
  }

  vector<Customer> Customers;
  vector<Location> Locations;

  instanceData problemData(&Customers, &Locations);

  try
  { Parser instance_parser(instance_file, &problemData); }
  catch(exception &e)
  { cout<<e.what()<<endl; }

  Simulator* simulator = new Simulator(&problemData);
  Controller* controller = new Controller(0, &problemData);

  simulator->registerObserver(controller);

  thread simu_thread(&Simulator::exec, simulator);
  thread contr_thread(&Controller::exec, controller);

  simu_thread.join();
  contr_thread.join();

  cout<<"END OF PROCESS\n";
}
