#include "../Headers/Solver.h"

Solver::Solver(instanceData* data) : problemdata(data)
{
  customers = data->customers;
  locations = data->locations;
  solution = new Solution(this);
}

int Solver::solve(vector<int>* ins_locations, vector<Route*>* list, int current_time)
{
  bool feasible;
  int idx_vehicles;

  Route* empty = nullptr;

  solution->routing_plan.clear();
  solution->planned_visits.clear();

  //current planned routes for vehicles at the depot
  for(Route* route : *list)
    solution->routing_plan.push_back(route);
  idx_vehicles = solution->routing_plan.size(); //the first idx_vehicles hlod the current routes of vehicles
  for(Route* r : solution->routing_plan)
    if(r->getLength()==0)
    {
      empty = r;
      break;
    }
  if(empty != nullptr)
    solution->routing_plan.push_back(empty);
  else//no empty route available...
  {
    int last_loc = locations->size()-1; //FIXME
    solution->routing_plan.push_back(new Route(problemdata));  //start with an empty route
    // solution->routing_plan.back()->start_depot.customer = 0; //FIXME
    // solution->routing_plan.back()->start_depot.location = 0;  //FIXME
    solution->routing_plan.back()->start_depot.service.set(current_time, locations->at(0).service.getEnd());
    // solution->routing_plan.back()->end_depot.customer = 16; //FIXME
    // solution->routing_plan.back()->end_depot.location = last_loc;  //FIXME
    solution->routing_plan.back()->end_depot.service.set(current_time, locations->at(last_loc).service.getEnd());
  }

  // for(Route* ro : solution->routing_plan)
  //   cout<<ro<<" ";
  // cout<<endl;

  for(int loc : *ins_locations)
  {
    solution->planned_visits.push_back(new nodeInfo(locations->at(loc).customer, locations->at(loc).id));
    solution->planned_visits.back()->service = locations->at(loc).service;

    // cout<<"Inserting "<<locations->at(loc).id<<" nodeInfo*="<<solution->planned_visits.back()<<endl;
  }

  for(nodeInfo* node : solution->planned_visits)
  {
    while(!insertions_pq.empty())
      insertions_pq.pop();

    // cout<<"Inserting "<<node->customer<<"("<<node->location<<")\n";
    for(Route* route : solution->routing_plan)
      feasible = route->evaluateInsertion(node, insertions_pq);
    if(!insertions_pq.empty())
    {
      // cout<<"inserting at route: "<<insertions_pq.top().route<<endl;
      if(insertions_pq.top().route->getLength() == 0)  //inserting in an empty route, add a new one to the pool
      {
        int last_loc = locations->size()-1; //FIXME
        solution->routing_plan.push_back(new Route(problemdata));  //start with an empty route
        // solution->routing_plan.back()->start_depot.customer = 0; //FIXME
        // solution->routing_plan.back()->start_depot.location = 0;  //FIXME
        solution->routing_plan.back()->start_depot.service.set(current_time, locations->at(0).service.getEnd());
        // solution->routing_plan.back()->end_depot.customer = 16; //FIXME
        // solution->routing_plan.back()->end_depot.location = last_loc;  //FIXME
        solution->routing_plan.back()->end_depot.service.set(current_time, locations->at(last_loc).service.getEnd());
      }
      insertions_pq.top().route->insertNode(node, insertions_pq.top().before, insertions_pq.top().after);
      insertions_pq.top().route->update_earliest();
      insertions_pq.top().route->update_latest();
      // cout<<*(insertions_pq.top().route)<<endl;
      customers->at(node->customer).service_route = insertions_pq.top().route;
    }
  }

  //return any newly created route. vehicle routes are modifyied in place in <list>
  for(int r=idx_vehicles; r<solution->routing_plan.size(); ++r)
    if(solution->routing_plan[r]->getLength() > 0)
    {
      list->push_back(solution->routing_plan[r]);
      cout<<*(solution->routing_plan[r])<<endl;
    }
  return solution->routing_plan.size() - idx_vehicles;
}

//computes an initial solution using all locations with earliest time 0
int Solver::initial(vector<Route*>* list)
{
  bool feasible;

  // for(Location& loc : *locations)
  // {
  //   if(loc.service.getStart()!=0 or loc.customer==problemdata->START_DEPOT or loc.customer==problemdata->TARGET_DEPOT)  //FIXME
  //     continue;
  //   // cout<<"include location:"<<loc.id<<endl;
  //   solution->planned_visits.push_back(new nodeInfo(loc.customer, loc.id));
  //   solution->planned_visits.back()->service = loc.service;
  // }

  int id = 0; //FIXME
  int l = 0;
  for(Customer& cust : *customers)
  {
    if(id==problemdata->START_DEPOT or id==problemdata->TARGET_DEPOT)
    {
      id++;
      continue;
    }

    l = cust.itinerary[cust.itinerary.size()-1];  //last one
    solution->planned_visits.push_back(new nodeInfo(locations->at(l).customer, locations->at(l).id));
    solution->planned_visits.back()->service = locations->at(l).service;
    id++;
  }

  for(nodeInfo* node : solution->planned_visits)
  {
    while(!insertions_pq.empty())
      insertions_pq.pop();

    // cout<<"Inserting "<<node.customer<<"("<<node.location<<")\n";
    for(Route* route : solution->routing_plan)
      feasible = route->evaluateInsertion(node, insertions_pq);
    if(!insertions_pq.empty())
    {
      if(insertions_pq.top().route->getLength() == 0)  //inserting in an empty route, add a new one to the pool
      {
        int last_loc = locations->size()-1; //FIXME
        solution->routing_plan.push_back(new Route(problemdata));  //start with an empty route
        // solution->routing_plan.back()->start_depot.customer = 0; //FIXME
        // solution->routing_plan.back()->start_depot.location = 0;  //FIXME
        solution->routing_plan.back()->start_depot.service.set(locations->at(0).service.getStart(), locations->at(0).service.getEnd());
        // solution->routing_plan.back()->end_depot.customer = 16; //FIXME
        // solution->routing_plan.back()->end_depot.location = last_loc;  //FIXME
        solution->routing_plan.back()->end_depot.service.set(locations->at(last_loc).service.getStart(), locations->at(last_loc).service.getEnd());
      }
      insertions_pq.top().route->insertNode(node, insertions_pq.top().before, insertions_pq.top().after);
      insertions_pq.top().route->update_earliest();
      insertions_pq.top().route->update_latest();
      // cout<<*(insertions_pq.top().route)<<endl;
      customers->at(node->customer).service_route = insertions_pq.top().route;
    }
  }

  //return non-empty routes
  for(Route* route : solution->routing_plan)
    if(route->getLength() > 0)
      list->push_back(route);
}

void Solver::checkInstance()
{
  Route* route = new Route(problemdata);
  nodeInfo* node;
  int last_loc = locations->size()-1; //FIXME

  int c = 0;
  for(Customer& cust : *customers)
  {
    cout<<"Customer:"<<c<<endl;
    for(int i=0; i<cust.itinerary.size(); ++i)
    {
      route = new Route(problemdata);
      // route->start_depot.customer = 0; //FIXME
      // route->start_depot.location = 0;  //FIXME
      route->start_depot.service.set(locations->at(0).service.getStart(), locations->at(0).service.getEnd());
      // route->end_depot.customer = 16; //FIXME
      // route->end_depot.location = last_loc;  //FIXME
      route->end_depot.service.set(locations->at(last_loc).service.getStart(), locations->at(last_loc).service.getEnd());

      while(!insertions_pq.empty())
        insertions_pq.pop();

      int loc = cust.itinerary[i];
      node = new nodeInfo(locations->at(loc).customer, locations->at(loc).id);

      // if(locations->at(loc).service.length() < 60)
      //   continue;

      node->service = locations->at(loc).service;

      if(i==0 or i==cust.itinerary.size()-1)
        route->start_depot.service.set(locations->at(0).service.getStart(), locations->at(0).service.getEnd());
      else
        route->start_depot.service.set(node->service.getStart(), locations->at(0).service.getEnd());

      route->evaluateInsertion(node, insertions_pq);
      if(!insertions_pq.empty())
      {
        insertions_pq.top().route->insertNode(node, insertions_pq.top().before, insertions_pq.top().after);
        insertions_pq.top().route->update_earliest();
        insertions_pq.top().route->update_latest();
        cout<<"Location "<<locations->at(loc).id<<" inserted!\n";
        cout<<*route<<endl;
      }
      else
        cout<<"Location "<<locations->at(loc).id<<" not feasible!\n";
    }
    cout<<"--------------------------\n";
    c++;
  }
}

// void Solver::update_solution()
// { solution->advance_time(); }

/*****************************/
// SOLUTION
/****************************/
Solution::Solution(Solver* so) : solver(so)
{
  planned_visits.reserve(100);
  routing_plan.reserve(100);
  int last_loc = solver->locations->size()-1; //FIXME
  routing_plan.push_back(new Route(solver->problemdata));  //start with an empty route
  // routing_plan.back()->start_depot.customer = 0; //FIXME
  // routing_plan.back()->start_depot.location = 0;  //FIXME
  routing_plan.back()->start_depot.service.set(solver->locations->at(0).service.getStart(), solver->locations->at(0).service.getEnd());
  // routing_plan.back()->end_depot.customer = 16; //FIXME
  // routing_plan.back()->end_depot.location = last_loc;  //FIXME
  routing_plan.back()->end_depot.service.set(solver->locations->at(last_loc).service.getStart(), solver->locations->at(last_loc).service.getEnd());
}

// void Solution::advance_time()
// {
//   for(Route* route: routing_plan)
//   {
//     if(route->step())
//     {
//       // route->update_earliest();
//       cout<<*route<<endl;
//       for(Customer& cust : *(solver->customers))
//         if(cust.service_route == route)
//           cust.serviced = true;
//
//       exit(1);
//     }
//   }
// }

// cout<<"Object succesfully constructed!\n";
 // for(int i=0; i<solver->locations->size(); ++i)
 // {
 //   cout<<"Location "<<i<<": ";
 //   cout<<solver->locations->at(i).service<<endl;
 // }
