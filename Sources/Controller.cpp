#include "../Headers/Controller.h"
#include <future>
#include <chrono>         // std::chrono::milliseconds
#include <thread>

void update_step(Controller* ctrl)
{
  this_thread::sleep_for(chrono::milliseconds(TIME_UNIT));
  ctrl->step();
  // cout<<"Current time: "<<simu->step()<<"\n";
  return;
}

Controller::Controller(int ID, instanceData* data) : id(ID), problemdata(data)
{
  int assigned_vehicles = 0;

  customers = data->customers;
  locations = data->locations;

  time_progress = 0;
  serviced_customers = 0;
  end_of_process = false;
  vehicles.reserve(100);
  routes.reserve(100);
  idx_locations.reserve(100);

  for(int i=0; i<FLEET_SIZE; ++i)
    vehicles.push_back(Vehicle(i, data->VEHIC_CAP));

  //TODO: compute an initial plan considering all requests with time wndows earliest than 0
  solver = new Solver(data);
  solver->initial(&routes);
  // solver->checkInstance();
  // exit(1);

  sort(routes.begin(), routes.end(), less_than_key());
  cout<<"Planned routes:\n";
  for(Route* route : routes)  //TODO: assigned at most FLEET_SIZE routes!
  {
    vehicles[assigned_vehicles].current_route = 0;
    vehicles[assigned_vehicles].planned_routes.push_back(route);
    assigned_vehicles++;
    route->assignCustomers();
    route->step();
    cout<<*route<<" wait:"<<route->getWaitingTime()<<endl;
  }
  exit(1);
}

void Controller::step()
{
  time_progress++;
  cout<<"#####################################\n";
  cout<<"Current time: "<<time_progress<<" serviced customers: "<<serviced_customers<<"\n";
  for(auto& trc : tracking)
  {
    // cout<<trc.first<<"("<<customers->at(trc.first).current_location<<") ";
    if(!trc.second.expired)
    {
      trc.second.time_budget--;
      if(trc.second.time_budget < 0)
        trc.second.expired = true;
    }
  }
  // cout<<endl;

  int aux;
  Route* route;
  for(Vehicle& veh : vehicles)
  {
    aux = veh.current_route;
    if(aux == veh.planned_routes.size()) //no route assigend to the vehicle yet...
      continue;
    route = veh.planned_routes[aux];
    cout<<"Vehicle "<<veh.ID<<"("<<route<<"): "<<*route;
    if(veh.dispatched)
    {
      veh.return_time--;
      if(veh.return_time <= 0)
      {
        cout<<" returned\n";
        veh.return_time = 0;
        veh.dispatched = false;
        veh.current_route++;
      }
      else
        cout<<" en route "<<veh.return_time<<"\n";
    }
    else
      cout<<" at depot\n";
    if(route->step())
    {//dispacht vehicle
      veh.dispatched = true;
      veh.return_time = route->getReturnTime() - time_progress;
      cout<<"|---Dispatch! "<<veh.return_time<<"\n";
      for(Customer& cust : *customers)
        if(cust.service_route == route)
        {
          serviced_customers++;
          cust.serviced = true;
        }
      route->assignCustomers();
    }
  }
  // solver->update_solution();
  cout<<endl;
}

void Controller::update(Status* sts)
{
  map<int, trackCustomer>::iterator it;
  int loc;
  int new_routes, veh_routes;
  // cout<<"New system state at "<<sts->time_epoch<<" !\n";

  idx_locations.clear();

  if(sts->event == END_OF_HORIZON)
    end_of_process = true;

  if(sts->event == CUSTOMER_LOCATION)
  {
    for(int cust : sts->available_customers)
    {
      //TODO: control whether consider the location e.g., time windows is too short
      loc = customers->at(cust).itinerary[customers->at(cust).current_location];
      it = tracking.find(cust);
      if(it!=tracking.end())
      {
        it->second.time_budget = locations->at(loc).service.length();
        it->second.expired = false;
      }
      else
        it = tracking.emplace(cust, trackCustomer(cust, locations->at(loc).service.length())).first;
      //tracking.push_back(trackCustomer(cust, locations->at(loc).service.length()));
      if(customers->at(it->first).service_route == nullptr and sts->time_epoch > 0)
        idx_locations.push_back(customers->at(cust).itinerary[customers->at(cust).current_location]);
    }
  }

  //try to accomodate new customers or new locations on planned routes for vehicle currently at the depot
  if(idx_locations.size() <= 0)
    return;

  // routes.resize(0);
  routes.clear();
  new_routes = 0;
  for(Vehicle& veh : vehicles)
  {
    if(veh.dispatched or veh.current_route == veh.planned_routes.size())//not at the depot...
      continue;
    routes.push_back(veh.planned_routes[veh.current_route]);
  }
  veh_routes = routes.size();
  new_routes = solver->solve(&idx_locations, &routes, sts->time_epoch) - veh_routes;
  //if any new route is created, assign to a vehicle currently in the depot
  for(int r=veh_routes; r<routes.size(); ++r)
  {
    for(Vehicle& veh : vehicles)  //TODO: vehicles not at the depot could also be assigned a route...
    {
      cout<<veh.ID<<"--->"<<veh.current_route<<" "<<veh.planned_routes.size()<<endl;
      if(veh.current_route == veh.planned_routes.size())
      {
        veh.planned_routes.push_back(routes[r]);
        if(routes[r]->getLatestDispatch() <= time_progress) //dispacth NOW
        {//dispacht vehicle
          veh.dispatched = true;
          veh.return_time = routes[r]->getReturnTime() - time_progress;
          cout<<"Vehicle "<<veh.ID<<"("<<routes[r]<<"): "<<*(routes[r]);
          cout<<"|---Dispatch! "<<veh.return_time<<"\n";
          for(Customer& cust : *customers)
            if(cust.service_route == routes[r])
            {
              cust.serviced = true;
              serviced_customers++;
            }
          routes[r]->assignCustomers();
        }
        // veh.planned_routes.clear();
        // veh.current_route++;
        // cout<<"Route assigned to vehicle!\n";
        // cout<<*(routes[r])<<endl;
        break;
      }
    }
  }
}

void Controller::exec()
{
  while(!end_of_process)
  {
    // this_thread::sleep_for(chrono::milliseconds(100));
    future<void> fut = std::async(update_step, this);
    fut.wait();
  }
  int cust_i = 0;
  for(Customer& cust : *customers)
  {
    if(cust.serviced == false)
      cout<<"Customer "<<cust_i<<" not serviced!\n";
    cust_i++;
  }

  cout<<"Controller has terminated!\n";
}
