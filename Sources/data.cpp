#include "../Headers/data.h"
#include <tuple>

bool operator <(const insertion& x, const insertion& y) {
    return tie(x.cost, x.before->customer) > tie(y.cost, y.before->customer); //> for increase order
}
