#include "../Headers/parser.h"

Parser::Parser(string file_name, instanceData* instance)//vector<Customer>* Customers, vector<Location>* Locations)
{
  ifstream instance_file(file_name.c_str());

  if(!instance_file.good())
  {
    FileNotFound_Exception ex;
    throw ex;
  }

  vector<Customer>* Customers = instance->customers;
  vector<Location>* Locations = instance->locations;

  string line;
  int value,tw1,tw2;
  int num_line = 0;
  int num_locs = 0;
  int NR_CUST = 0;
  int NR_LOC = 0;
  int TIME_HORIZON = 0;
  int VEHIC_CAP = 0;
  double coordx, coordy;

  getline(instance_file, line); //SKIP
  getline(instance_file, line); //SKIP

  getline(instance_file, line);  //PARAMS
  istringstream iss(line);
  iss >> NR_CUST;
  iss >> NR_LOC;
  iss >> TIME_HORIZON; //should be the same as the right hand side of the time window of the depots
  iss >> VEHIC_CAP;
  num_line++;

  getline(instance_file, line); //SKIP
  getline(instance_file, line); //SKIP
  getline(instance_file, line); //SKIP
  getline(instance_file, line); //SKIP

  for(int i=0; i<NR_CUST+2; ++i)  //customer schedules
  {
    getline(instance_file, line);
    replace(line.begin(), line.end(), ',', ' ');
    replace(line.begin(), line.end(), '[', ' ');
    replace(line.begin(), line.end(), ']', ' ');
    istringstream iss(line);
    iss >> value; //TODO: assuming provided ID is the same as the order (i)
    iss >> value; //capacity
    Customers->push_back(Customer(value));
    while(iss >> value)
    {
      Customers->at(i).itinerary.push_back(value);
      iss >> tw1;
      iss >> tw2;
      Locations->push_back(Location(value, i, 0, 0));  //coord to be set up later
      Locations->at(num_locs++).service.set(tw1,tw2);
      // cout<<"-----"<<value<<" "<<tw1<<" "<<tw2<<endl;
    }
    // cout<<line<<endl;
  }

  //customer locations
  getline(instance_file, line); //SKIP
  getline(instance_file, line); //SKIP
  getline(instance_file, line); //SKIP
  getline(instance_file, line); //SKIP

  for(int i=0; i<NR_LOC; ++i)
  {
    getline(instance_file, line);
    istringstream iss(line);
    iss >> value; //ID. should be the same as i
    iss >> coordx;
    iss>> coordy;
    Locations->at(i).xcoord = coordx;
    Locations->at(i).ycoord = coordy;
    // cout<<value<<" "<<coordx<<" "<<coordy<<endl;
  }

//  cout<<NR_CUST<<" "<<NR_LOC<<" "<<TIME_HORIZON<<" "<<VEHIC_CAP<<endl;

//  for(int i=0; i<Customers->size(); ++i)
//  {
//    cout<<Customers->at(i).capacity<<" ";
//    for(int j=0; j<Customers->at(i).itinerary.size(); ++j)
//      cout<<Customers->at(i).itinerary[j]<<" ";
//    cout<<endl;
//  }

//  for(int i=0; i<Locations->size(); ++i)
//  {
//    cout<<"Location "<<i<<": ";
//    cout<<Locations->at(i).service<<endl;
//  }

  instance_file.close();

  instance->NR_CUST = NR_CUST;
  instance->NR_LOC = NR_LOC;
  instance->TIME_HORIZON = TIME_HORIZON;
  instance->VEHIC_CAP = VEHIC_CAP;
  instance->START_DEPOT = 0;  //FIXME: first customer(location) is the start depot
  instance->TARGET_DEPOT = NR_CUST+1; //FIXME: last customer/location is target depot
}
