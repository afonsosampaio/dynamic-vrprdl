#include "../Headers/Simulator.h"
#include <future>
#include <chrono>         // std::chrono::milliseconds
#include <thread>

void update_step(Simulator* simu)
{
  this_thread::sleep_for(chrono::milliseconds(TIME_UNIT));
  simu->step();
  // cout<<"Current time: "<<simu->step()<<"\n";
  return;
}

Simulator::Simulator(instanceData* data) : problemdata(data)
{
  customers = data->customers;
  locations = data->locations;
  time_progress = 0;
  end_of_process = false;
  system_status.available_customers.reserve(100); //NOTE:ideally, the maximum number of new customers per epoch
  system_status.travelling_customers.reserve(100); //NOTE:ideally, the maximum number of new customers per epoch
}

int Simulator::step() {
  return ++time_progress;
}

/*Observer-Subject interface*/
void Simulator::registerObserver(Observer* obs)
{
  observers.push_back(obs);
}

void Simulator::removeObserver(Observer* obs)
{
  auto iterator = std::find(observers.begin(), observers.end(), obs);
  if (iterator != observers.end())
     observers.erase(iterator); // remove the observer
}

void Simulator::notifyObservers(Status* sts)
{
  for (Observer* observer : observers)
    observer->update(sts);
}

void Simulator::update_system()
{
  int loc;
  bool update_controller = false;

  system_status.time_epoch = time_progress;
  system_status.available_customers.clear();
  system_status.travelling_customers.clear();

  if(time_progress >= problemdata->TIME_HORIZON)
  {
    end_of_process = true;
    system_status.event = END_OF_HORIZON;
    notifyObservers(&system_status);
    return;
  }

  for(int i=1; i<customers->size()-1; ++i)  //exclude first and last customers: depot
  {
    if(customers->at(i).serviced) //do not inform to the controller the itinerary of already serviced customers
      continue;

    loc = customers->at(i).itinerary[customers->at(i).current_location];

    //arrived at a location
    if(locations->at(loc).service.getStart() <= time_progress and !locations->at(loc).released)
    {
      locations->at(loc).released = true;
      customers->at(i).t = 0.0;
      // cout<<"#"<<time_progress<<" ";
      cout<<"Customer "<<i<<" at "<<loc<<" tw:"<<locations->at(loc).service<<"\n";
      update_controller = true;
      system_status.event = CUSTOMER_LOCATION;
      system_status.available_customers.push_back(i);
    }

    //travelling to a location
    if(locations->at(loc).service.getStart() >= time_progress and  !locations->at(loc).released and !locations->at(loc).departed)
    {
      customers->at(i).t += customers->at(i).step;
      //TODO: update current location
      system_status.travelling_customers.push_back(i);
    }

    //departed a location
    if(locations->at(loc).service.getEnd() <= time_progress and !locations->at(loc).departed and locations->at(loc).released)
    {
      locations->at(loc).departed = true;
      cout<<"Customer "<<i<<" departed from "<<loc<<" tw:"<<locations->at(loc).service<<"\n";

      //moving
      customers->at(i).xcoord = locations->at(loc).xcoord;
      customers->at(i).ycoord = locations->at(loc).ycoord;

      customers->at(i).current_location++;
      loc = customers->at(i).itinerary[customers->at(i).current_location];

      // cout<<"Travel for "<<distance(customers->at(i).xcoord,customers->at(i).ycoord,locations->at(loc).xcoord,locations->at(loc).ycoord)<<"\n";
    }
  }

  if(update_controller)
    notifyObservers(&system_status);
}

/*Main loop*/
void Simulator::exec()
{
  update_system();
  while(!end_of_process)
  {
    future<void> fut = std::async(update_step, this);
    fut.wait();
    //release new information
    update_system();
  }
  cout<<"Simulation has finished!\n";
}
