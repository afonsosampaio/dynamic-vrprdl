CC = g++
SRC = $(shell find ./Sources -name *.cpp)

FLAGS = -O3 -fPIC -fexceptions -g -pthread
VERSION = -std=c++11
SANITIZERS = -fsanitize=address

pdpt:
	$(CC) $(VERSION) $(FLAGS) $(SRC) -o ./Build/main

debug:
	$(CC) $(VERSION) $(SANITIZERS) $(FLAGS) $(SRC) -o ./Build/main_dbg
